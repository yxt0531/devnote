extends Node2D


enum Screen {
	TEXT_EDIT, HELP
}

const HELP_BAR_TEXT_IN_TEXT_EDIT = "F1-HELP   F2-SAVE   F3-FULLSCREEN   F5-QUIT"
const HELP_BAR_TEXT_IN_HELP = "F1-RETURN"

var save_prompt_timer: float = 0

var current_screen = Screen.TEXT_EDIT


func _ready():
	$LineEdit.grab_focus()
	

func _process(delta):
	_process_shortcut_key_input(delta)
	

func _process_shortcut_key_input(delta):
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
	if Input.is_action_just_pressed("escape"):
		get_tree().change_scene("res://scenes/note_select.tscn")
	elif Input.is_action_just_pressed("save"):
		save_current()
	elif Input.is_action_just_pressed("help"):
		if current_screen == Screen.TEXT_EDIT:
			current_screen = Screen.HELP
		else:
			current_screen = Screen.TEXT_EDIT
			
	match current_screen:
		Screen.TEXT_EDIT:
			$TextLog.visible = true
			$LineEdit.visible = true
			$LabelHelpText.visible = false
			# $LabelHelpBar.text = HELP_BAR_TEXT_IN_TEXT_EDIT
			
			$LineEdit.grab_focus()
			
			_process_save_prompt(delta)
		Screen.HELP:
			$TextLog.visible = false
			$LineEdit.visible = false
			$LabelHelpText.visible = true
			$LabelHelpBar.text = HELP_BAR_TEXT_IN_HELP


func _process_save_prompt(delta):
	if save_prompt_timer > 0:
		save_prompt_timer -= delta
		$LabelHelpBar.text = "SAVED"
	else:
		$LabelHelpBar.text = HELP_BAR_TEXT_IN_TEXT_EDIT
		
		
func save_current():
	if $TextLog.text.length() > 0:
		var note_key: String = $TextLog.get_line(0).sha1_text()
		var note_value: Dictionary = {}
		
		note_value.title = $TextLog.get_line(0)
		note_value.text = $TextLog.text
		
		NoteDb.notes[note_key] = note_value
	
		NoteDb.save()
		
		save_prompt_timer = 2
		
		
func _on_LineEdit_text_entered(new_text):
	var print_text: String = new_text.replace("\n","\\n")
	OS.execute("bash", ["print.sh", print_text], false)
	
	$TextLog.text += new_text + "\n"
	$TextLog.cursor_set_line($TextLog.get_line_count())
	$LineEdit.text = ""

