extends TextEdit


var line_light_fade_rects: Array = []

var light_colors: Array = []

var current_light_color_idxes: Array = []


func _ready():
	grab_focus() # auto set focus
	
	light_colors.append($LightLine8.color)
	light_colors.append($LightLine1.color)
	light_colors.append($LightLine2.color)
	light_colors.append($LightLine3.color)
	light_colors.append($LightLine4.color)
	light_colors.append($LightLine5.color)
	light_colors.append($LightLine6.color)
	light_colors.append($LightLine7.color)
	
	line_light_fade_rects.append($LightLineFadeRect1)
	line_light_fade_rects.append($LightLineFadeRect2)
	line_light_fade_rects.append($LightLineFadeRect3)
	line_light_fade_rects.append($LightLineFadeRect4)
	line_light_fade_rects.append($LightLineFadeRect5)
	line_light_fade_rects.append($LightLineFadeRect6)
	line_light_fade_rects.append($LightLineFadeRect7)
	line_light_fade_rects.append($LightLineFadeRect8)
	
	for c in light_colors:
		current_light_color_idxes.append(0)
	
	$LightLine1.color = light_colors[0]
	$LightLine2.color = light_colors[0]
	$LightLine3.color = light_colors[0]
	$LightLine4.color = light_colors[0]
	$LightLine5.color = light_colors[0]
	$LightLine6.color = light_colors[0]
	$LightLine7.color = light_colors[0]
	$LightLine8.color = light_colors[0]
	
	if NoteDb.current_note.has("title"):
		pass
	if NoteDb.current_note.has("text"):
		text = NoteDb.current_note["text"]
	if NoteDb.current_note.has("line_color_1"):
		current_light_color_idxes[0] = NoteDb.current_note["line_color_1"]
		$LightLine1.color = light_colors[current_light_color_idxes[0]]
	if NoteDb.current_note.has("line_color_2"):
		current_light_color_idxes[1] = NoteDb.current_note["line_color_2"]
		$LightLine2.color = light_colors[current_light_color_idxes[1]]
	if NoteDb.current_note.has("line_color_3"):
		current_light_color_idxes[2] = NoteDb.current_note["line_color_3"]
		$LightLine3.color = light_colors[current_light_color_idxes[2]]
	if NoteDb.current_note.has("line_color_4"):
		current_light_color_idxes[3] = NoteDb.current_note["line_color_4"]
		$LightLine4.color = light_colors[current_light_color_idxes[3]]
	if NoteDb.current_note.has("line_color_5"):
		current_light_color_idxes[4] = NoteDb.current_note["line_color_5"]
		$LightLine5.color = light_colors[current_light_color_idxes[4]]
	if NoteDb.current_note.has("line_color_6"):
		current_light_color_idxes[5] = NoteDb.current_note["line_color_6"]
		$LightLine6.color = light_colors[current_light_color_idxes[5]]
	if NoteDb.current_note.has("line_color_7"):
		current_light_color_idxes[6] = NoteDb.current_note["line_color_7"]
		$LightLine7.color = light_colors[current_light_color_idxes[6]]
	if NoteDb.current_note.has("line_color_8"):
		current_light_color_idxes[7] = NoteDb.current_note["line_color_8"]
		$LightLine8.color = light_colors[current_light_color_idxes[7]]


func _process(delta):
	_process_light_light_fade_out(delta)
	
	_process_input_toggle_line_light_color()
	_process_input_text_edit()
	_process_dpad_control()


func _process_light_light_fade_out(delta):
	for c_rect in line_light_fade_rects:
		if c_rect is ColorRect and c_rect.modulate.a > 0:
			c_rect.modulate.a -= delta


func _process_input_toggle_line_light_color():
	if Input.is_action_just_pressed("set_line_light_color_1"):
		var toggle_idx = 0
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine1.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect1.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_2"):
		var toggle_idx = 1
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine2.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect2.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_3"):
		var toggle_idx = 2
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine3.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect3.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_4"):
		var toggle_idx = 3
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine4.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect4.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_5"):
		var toggle_idx = 4
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine5.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect5.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_6"):
		var toggle_idx = 5
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine6.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect6.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_7"):
		var toggle_idx = 6
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine7.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect7.modulate.a = 1
	elif Input.is_action_just_pressed("set_line_light_color_8"):
		var toggle_idx = 7
		
		if current_light_color_idxes[toggle_idx] < light_colors.size() - 1:
			current_light_color_idxes[toggle_idx] += 1
		else:
			current_light_color_idxes[toggle_idx] = 0
		
		$LightLine8.color = light_colors[current_light_color_idxes[toggle_idx]]
		$LightLineFadeRect8.modulate.a = 1


func _process_input_text_edit():
	if Input.is_action_just_pressed("text_editor_copy"):
		copy()
	elif Input.is_action_just_pressed("text_editor_cut"):
		cut()
	elif Input.is_action_just_pressed("text_editor_paste"):
		paste()
	elif Input.is_action_just_pressed("text_editor_select_all"):
		select_all()
	elif Input.is_action_just_pressed("text_editor_undo"):
		undo()
	elif Input.is_action_just_pressed("text_editor_redo"):
		redo()


func _process_dpad_control():
	if DtDpadMonitor.is_up_pressed():
		if cursor_get_line() > 0:
			cursor_set_line(cursor_get_line() - 1)
	elif DtDpadMonitor.is_down_pressed():
		if cursor_get_line() < get_line_count() - 1:
			cursor_set_line(cursor_get_line() + 1)
	elif DtDpadMonitor.is_left_pressed():
		if cursor_get_column() > 0:
			cursor_set_column(cursor_get_column() - 1)
	elif DtDpadMonitor.is_right_pressed():
		if cursor_get_column() < get_line(cursor_get_line()).length():
			cursor_set_column(cursor_get_column() + 1)
