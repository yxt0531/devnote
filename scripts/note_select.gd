extends Node2D


var notes_keys = []
var notes_title_pos = []


func _ready():
	$NoteList.grab_focus()
	
	for n_key in NoteDb.notes.keys():
		notes_keys.append(n_key)
		$NoteList.add_item(NoteDb.notes[n_key].title)
	
	NoteDb.current_note.clear()
	

func _process(_delta):
	_process_shortcut_key_input()
	
	if DtDpadMonitor.is_up_pressed():
		var ev = InputEventAction.new()
		ev.action = "ui_up"
		ev.pressed = true
		Input.parse_input_event(ev)
	elif DtDpadMonitor.is_down_pressed():
		var ev = InputEventAction.new()
		ev.action = "ui_down"
		ev.pressed = true
		Input.parse_input_event(ev)
	
	
func _process_shortcut_key_input():
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()
	elif Input.is_action_just_pressed("new"):
		get_tree().change_scene("res://scenes/text_editor.tscn")
	elif Input.is_action_just_pressed("ui_accept"):
		if $NoteList.is_anything_selected():
			var selected_idx = 0
			while selected_idx < notes_keys.size():
				if $NoteList.is_selected(selected_idx):
					break
				selected_idx += 1

			NoteDb.current_note = NoteDb.notes[notes_keys[selected_idx]].duplicate(true)
			get_tree().change_scene("res://scenes/text_editor.tscn")
	elif Input.is_action_just_pressed("delete"):
		if $NoteList.is_anything_selected():
			var selected_idx = 0
			while selected_idx < notes_keys.size():
				if $NoteList.is_selected(selected_idx):
					break
				selected_idx += 1
			NoteDb.notes.erase(notes_keys[selected_idx])
			NoteDb.save()
			
			get_tree().change_scene("res://scenes/note_select.tscn")
	elif Input.is_action_just_pressed("typewriter"):
		get_tree().change_scene("res://scenes/type_writer.tscn")
			
