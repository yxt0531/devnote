extends Node


export var save_path = "user://notes.json"

var current_note: Dictionary = {}
var notes: Dictionary = {}

var save_file = File.new()

var auto_save: bool = true


func _ready():
	load_save()
	

func save():
	if save_file.file_exists(save_path):
		print("file exist: " + save_path + ", overwritting...")
	
	save_file.open(save_path, File.WRITE)
	save_file.store_string(to_json(notes))
	save_file.close()
	
	print("saved: " + save_path)
	
	
func save_quit():
	save()
	get_tree().quit()
	
	
func load_save():
	if save_file.file_exists(save_path):
		save_file.open(save_path, File.READ)
		notes = parse_json(save_file.get_line())
		save_file.close()

	else:
		print("unable to load, save file does not exist: " + save_path)
