extends Node


const PRESS_STRENGTH = 0.75
const RELEASE_STRENGTH = 0.25

var up_pressed: bool = false
var up_released: bool = true
var up_popped: bool = false

var down_pressed: bool = false
var down_released: bool = true
var down_popped: bool = false

var left_pressed: bool = false
var left_released: bool = true
var left_popped: bool = false

var right_pressed: bool = false
var right_released: bool = true
var right_popped: bool = false


func _process(_delta):
	if Input.get_action_strength("ls_up") > PRESS_STRENGTH:
		up_pressed = true
		up_released = false
	elif Input.get_action_strength("ls_up") < RELEASE_STRENGTH:
		up_pressed = false
		up_released = true
		up_popped = false
		
	if Input.get_action_strength("ls_down") > PRESS_STRENGTH:
		down_pressed = true
		down_released = false
	elif Input.get_action_strength("ls_down") < RELEASE_STRENGTH:
		down_pressed = false
		down_released = true
		down_popped = false
		
	if Input.get_action_strength("ls_left") > PRESS_STRENGTH:
		left_pressed = true
		left_released = false
	elif Input.get_action_strength("ls_left") < RELEASE_STRENGTH:
		left_pressed = false
		left_released = true
		left_popped = false
	
	if Input.get_action_strength("ls_right") > PRESS_STRENGTH:
		right_pressed = true
		right_released = false
	elif Input.get_action_strength("ls_right") < RELEASE_STRENGTH:
		right_pressed = false
		right_released = true
		right_popped = false
		
	

func is_up_pressed() -> bool:
	if not up_popped and Input.get_action_strength("ls_up") > PRESS_STRENGTH:
		up_popped = true
		return true
	else:
		return false
		
		
func is_down_pressed() -> bool:
	if not down_popped and Input.get_action_strength("ls_down") > PRESS_STRENGTH:
		down_popped = true
		return true
	else:
		return false


func is_left_pressed() -> bool:
	if not left_popped and Input.get_action_strength("ls_left") > PRESS_STRENGTH:
		left_popped = true
		return true
	else:
		return false
		
		
func is_right_pressed() -> bool:
	if not right_popped and Input.get_action_strength("ls_right") > PRESS_STRENGTH:
		right_popped = true
		return true
	else:
		return false
