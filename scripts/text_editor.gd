extends Node2D


enum Screen {
	TEXT_EDIT, HELP
}

var current_screen = Screen.TEXT_EDIT

const HELP_BAR_TEXT_IN_TEXT_EDIT = "F1-HELP   F2-SAVE   F3-FULLSCREEN   F4-PRINT   F5-QUIT"
const HELP_BAR_TEXT_IN_HELP = "F1-RETURN"

var save_prompt_timer: float = 0
var autosave_prompt_timer: float = 0

func _process(delta):
	_process_label_caret_pos()
	
	_process_shortcut_key_input(delta)
	

func _process_label_caret_pos():
	$LabelCurrentCaretPos.text = str($TextEdit.cursor_get_line()) + "," + str($TextEdit.cursor_get_column())


func _process_save_prompt(delta):
	if save_prompt_timer > 0:
		save_prompt_timer -= delta
		$LabelHelpBar.text = "SAVED"
	else:
		$LabelHelpBar.text = HELP_BAR_TEXT_IN_TEXT_EDIT
		
func _process_autosave_prompt(delta):
	if autosave_prompt_timer > 0:
		autosave_prompt_timer -= delta
		if NoteDb.auto_save:
			$LabelHelpBar.text = "AUTOSAVE ON"
		else:
			$LabelHelpBar.text = "AUTOSAVE OFF"
	else:
		$LabelHelpBar.text = HELP_BAR_TEXT_IN_TEXT_EDIT


func _process_shortcut_key_input(delta):
	if Input.is_action_just_pressed("quit"):
		if NoteDb.auto_save:
			NoteDb.save_quit()
		else:
			get_tree().quit()
	elif Input.is_action_just_pressed("help"):
		if current_screen == Screen.TEXT_EDIT:
			current_screen = Screen.HELP
		else:
			current_screen = Screen.TEXT_EDIT
	elif Input.is_action_just_pressed("save"):
		save_current()
	elif Input.is_action_just_pressed("toggle_auto_save"):
		NoteDb.auto_save = not NoteDb.auto_save
		autosave_prompt_timer = 2
			
	match current_screen:
		Screen.TEXT_EDIT:
			$TextEdit.visible = true
			$LabelHelpText.visible = false
			$LabelHelpBar.text = HELP_BAR_TEXT_IN_TEXT_EDIT
			
			$TextEdit.grab_focus()
			
			if Input.is_action_just_pressed("escape"):
				if NoteDb.auto_save:
					save_current()
				get_tree().change_scene("res://scenes/note_select.tscn")
			elif Input.is_action_just_pressed("print"):
				var new_text: String = $TextEdit.text.replace("\n","\\n")
				OS.execute("bash", ["print.sh", new_text])
			
			_process_save_prompt(delta)
			_process_autosave_prompt(delta)
		Screen.HELP:
			$TextEdit.visible = false
			$LabelHelpText.visible = true
			$LabelHelpBar.text = HELP_BAR_TEXT_IN_HELP


func save_current():
	if $TextEdit.text.length() > 0:
		var note_key: String = $TextEdit.get_line(0).sha1_text()
		var note_value: Dictionary = {}
		
		note_value.title = $TextEdit.get_line(0)
		note_value.text = $TextEdit.text
		
		note_value.line_color_1 = $TextEdit.current_light_color_idxes[0]
		note_value.line_color_2 = $TextEdit.current_light_color_idxes[1]
		note_value.line_color_3 = $TextEdit.current_light_color_idxes[2]
		note_value.line_color_4 = $TextEdit.current_light_color_idxes[3]
		note_value.line_color_5 = $TextEdit.current_light_color_idxes[4]
		note_value.line_color_6 = $TextEdit.current_light_color_idxes[5]
		note_value.line_color_7 = $TextEdit.current_light_color_idxes[6]
		note_value.line_color_8 = $TextEdit.current_light_color_idxes[7]
		
		NoteDb.notes[note_key] = note_value
	
		NoteDb.save()
		
		save_prompt_timer = 2
