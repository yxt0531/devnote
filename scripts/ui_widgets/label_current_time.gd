extends Label


var hour: String = "01"
var minute: String = "27"
var ampm: String = "AM"


func _process(_delta):
	if OS.get_time().hour >= 12:
		ampm = "PM"
	else:
		ampm = "AM"
	
	if OS.get_time().hour == 0:
		hour = "12"
	elif OS.get_time().hour > 12:
		hour = str(OS.get_time().hour - 12)
	else:
		hour = str(OS.get_time().hour)
	
	minute = str(OS.get_time().minute)

	if hour.length() < 2:
		hour = "0" + hour
	if minute.length() < 2:
		minute = "0" + minute
		
	text = hour + ":" + minute + ampm
